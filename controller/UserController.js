const User = require('../model/User')

const userController = {
  userList: [
    { id: 1, name: 'Phongsathorn', gender: 'M' },
    { id: 2, name: 'phongsa', gender: 'M' }
  ],
  lastId: 3,
  addUser (req, res, next) {
    const payload = req.body
    User.create(payload)
      .then(function (user) {
        res.json(user)
      })
      .catch(function (err) {
        res.status(500).send(err)
      })
  },
  async updateUser (req, res, next) {
    const payload = req.body
    try {
      const user = await User.updateOne({ _id: payload._id }, payload)
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deleteUser (req, res, next) {
    const { id } = req.params
    // res.json(usersController.deleteUser(id))
    try {
      const user = await User.deleteOne({ _id: id })
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getUsers (req, res, next) {
    try {
      const users = await User.find({})
      res.json(users)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  getUser (req, res, next) {
    const { id } = req.params
    // res.json(usersController.getUser(id))
    User.findById(id)
      .then(function (user) {
        res.json(user)
      })
      .catch(function (err) {
        res.status(500).send(err)
      })
  }
}

module.exports = userController
